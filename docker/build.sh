#!/bin/bash
#=======================================================================================
#title          :Unlock docs build image
#description    :Build container for unlock documentation
#author         :Bart Nijsse & Jasper Koehorst
#date           :2022
#version        :0.0.1
#=======================================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

docker login registry.gitlab.com

docker build -t registry.gitlab.com/m-unlock/m-unlock.gitlab.io $DIR

docker push registry.gitlab.com/m-unlock/m-unlock.gitlab.io